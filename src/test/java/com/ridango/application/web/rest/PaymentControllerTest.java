package com.ridango.application.web.rest;

import com.ridango.application.db.service.PaymentService;
import com.ridango.application.dto.PaymentRequestDto;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Set;

import static com.ridango.application.common.enumerations.ErrorDescription.AMOUNT_INCORRECT;
import static com.ridango.application.common.enumerations.ErrorDescription.AMOUNT_INVALID_FORMAT;
import static com.ridango.application.common.enumerations.ErrorDescription.CAN_NOT_BE_EMPTY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.springframework.http.ResponseEntity.ok;

@ExtendWith(MockitoExtension.class)
class PaymentControllerTest {
  @Mock
  private PaymentService paymentService;
  @InjectMocks
  private PaymentController paymentController;

  private Validator validator;

  @BeforeEach
  public void setup() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  void makePayment_whenGivenPaymentRequestDto_shouldReturnStatusOkAndMakePayment() {
    PaymentRequestDto paymentRequestDto = createPaymentRequestDto(1L, 2L, new BigDecimal("100.00"));
    doNothing().when(paymentService).makePayment(paymentRequestDto);

    ResponseEntity<Void> result = paymentController.makePayment(paymentRequestDto);

    assertEquals(ok().build(), result);
  }

  @Test
  void makePayment_whenGivenSenderIsNull_senderValidationShouldFail() {
    PaymentRequestDto invalidPaymentRequest = createPaymentRequestDto(null, 2L, new BigDecimal("100.00"));

    Set<ConstraintViolation<PaymentRequestDto>> violations = validator.validate(invalidPaymentRequest);

    assertAll(
            () -> assertThat(violations).hasSize(1),
            () -> assertThat(violations.iterator().next().getMessage()).isEqualTo(CAN_NOT_BE_EMPTY),
            () -> assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("senderAccountId")
    );
  }

  @Test
  void makePayment_whenGivenReceiverIsNull_receiverValidationShouldFail() {
    PaymentRequestDto invalidPaymentRequest = createPaymentRequestDto(1L, null, new BigDecimal("100.00"));

    Set<ConstraintViolation<PaymentRequestDto>> violations = validator.validate(invalidPaymentRequest);

    assertAll(
            () -> assertThat(violations).hasSize(1),
            () -> assertThat(violations.iterator().next().getMessage()).isEqualTo(CAN_NOT_BE_EMPTY),
            () -> assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("receiverAccountId")
    );
  }

  @Test
  void makePayment_whenGivenAmountIsNull_amountValidationShouldFail() {
    PaymentRequestDto invalidPaymentRequest = createPaymentRequestDto(1L, 2L, null);

    Set<ConstraintViolation<PaymentRequestDto>> violations = validator.validate(invalidPaymentRequest);

    assertAll(
            () -> assertThat(violations).hasSize(1),
            () -> assertThat(violations.iterator().next().getMessage()).isEqualTo(CAN_NOT_BE_EMPTY),
            () -> assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("amount")
    );
  }

  @Test
  void makePayment_whenGivenTooSmallAmount_amountValidationShouldFail() {
    PaymentRequestDto invalidPaymentRequest = createPaymentRequestDto(1L, 2L, new BigDecimal("0.00"));

    Set<ConstraintViolation<PaymentRequestDto>> violations = validator.validate(invalidPaymentRequest);

    assertAll(
            () -> assertThat(violations).hasSize(1),
            () -> assertThat(violations.iterator().next().getMessage()).isEqualTo(AMOUNT_INCORRECT),
            () -> assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("amount")
    );
  }

  @Test
  void makePayment_whenGivenAmountIntegerIncorrect_amountValidationShouldFail() {
    PaymentRequestDto invalidPaymentRequest = createPaymentRequestDto(
            1L,
            2L,
            BigDecimal.TEN.pow(17).add(new BigDecimal("0.01")));

    Set<ConstraintViolation<PaymentRequestDto>> violations = validator.validate(invalidPaymentRequest);

    assertAll(
            () -> assertThat(violations).hasSize(1),
            () -> assertThat(violations.iterator().next().getMessage()).isEqualTo(AMOUNT_INVALID_FORMAT),
            () -> assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("amount")
    );
  }

  @Test
  void makePayment_whenGivenAmountFractionIncorrect_amountValidationShouldFail() {
    PaymentRequestDto invalidPaymentRequest = createPaymentRequestDto(
            1L,
            2L,
            new BigDecimal("0.011"));

    Set<ConstraintViolation<PaymentRequestDto>> violations = validator.validate(invalidPaymentRequest);

    assertAll(
            () -> assertThat(violations).hasSize(1),
            () -> assertThat(violations.iterator().next().getMessage()).isEqualTo(AMOUNT_INVALID_FORMAT),
            () -> assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("amount")
    );
  }

  private static PaymentRequestDto createPaymentRequestDto(Long senderAccountId, Long receiverAccountId, BigDecimal amount) {
    return PaymentRequestDto.builder()
            .senderAccountId(senderAccountId)
            .receiverAccountId(receiverAccountId)
            .amount(amount)
            .build();
  }
}
