package com.ridango.application.web.rest;

import com.ridango.application.db.service.AccountService;
import com.ridango.application.dto.AccountDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class AccountControllerTest {
  @Mock
  private AccountService accountService;
  @InjectMocks
  private AccountController accountController;

  @Test
  void getAccountById_whenGivenId_shouldReturnStatusOkAndAccountDto() {
    Long accountId = 1L;
    AccountDto accountDto = AccountDto.builder()
            .name("Mari Maasikas")
            .balance(new BigDecimal("100.00"))
            .build();
    when(accountService.getAccountDtoById(accountId)).thenReturn(accountDto);

    ResponseEntity<AccountDto> result = accountController.getAccountById(accountId);

    assertAll(
            () -> assertEquals(HttpStatus.OK, result.getStatusCode()),
            () -> assertEquals(accountDto, result.getBody())
    );
  }
}