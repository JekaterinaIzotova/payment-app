package com.ridango.application.db.service;

import com.ridango.application.db.domain.Account;
import com.ridango.application.db.domain.Payment;
import com.ridango.application.db.repository.PaymentRepository;
import com.ridango.application.dto.PaymentRequestDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static com.ridango.application.common.enumerations.AccountType.RECEIVER_ACCOUNT;
import static com.ridango.application.common.enumerations.AccountType.SENDER_ACCOUNT;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class PaymentServiceTest {
  @Mock
  private PaymentRepository paymentRepository;
  @Mock
  private AccountService accountService;
  @InjectMocks
  private PaymentService paymentService;

  ArgumentCaptor<Payment> paymentCaptor = ArgumentCaptor.forClass(Payment.class);

  @Test
  void makePayment_whenGivenPaymentRequest_shouldSuccessfullyMakePayment() {
    Long senderAccountId = 1L;
    Long receiverAccountId = 2L;
    BigDecimal requestedAmount = new BigDecimal("20.00"); 
    PaymentRequestDto paymentRequestDto = PaymentRequestDto.builder()
            .senderAccountId(senderAccountId)
            .receiverAccountId(receiverAccountId)
            .amount(requestedAmount)
            .build();    
    Account senderAccount = Account.builder()
            .id(senderAccountId)        
            .balance(new BigDecimal("100.00"))
            .build();
    Account receiverAccount = Account.builder()
            .id(receiverAccountId)
            .balance(new BigDecimal("50.00"))
            .build();

    when(accountService.getAccountById(senderAccountId, SENDER_ACCOUNT)).thenReturn(senderAccount);
    when(accountService.getAccountById(receiverAccountId, RECEIVER_ACCOUNT)).thenReturn(receiverAccount);

    paymentService.makePayment(paymentRequestDto);

    assertAll(
            () -> verify(accountService).validateSenderAccountBalance(senderAccount, requestedAmount),
            () -> verify(accountService).updateBalances(senderAccount, receiverAccount, requestedAmount),
            () -> verify(paymentRepository).save(paymentCaptor.capture())
    );

    Payment capturedPayment = paymentCaptor.getValue();

    assertAll(
            () -> assertEquals(senderAccount, capturedPayment.getSenderAccount()),
            () -> assertEquals(receiverAccount, capturedPayment.getReceiverAccount()),
            () -> assertNotNull(capturedPayment.getTimestamp())
    );
  }
}