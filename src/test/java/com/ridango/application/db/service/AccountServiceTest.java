package com.ridango.application.db.service;

import com.ridango.application.common.exceptions.AccountNotFoundException;
import com.ridango.application.common.exceptions.PaymentException;
import com.ridango.application.db.domain.Account;
import com.ridango.application.db.repository.AccountRepository;
import com.ridango.application.dto.AccountDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {
  @Mock
  private AccountRepository accountRepository;
  @InjectMocks
  private AccountService accountService;

  @Test
  void getAccountDtoById_whenGivenId_shouldReturnAccountDto() {
    Long accountId = 1L;
    String accountName = "Mari Maasikas";
    BigDecimal accountBalance = new BigDecimal("100.00");
    when(accountRepository.findById(accountId))
            .thenReturn(Optional.of(Account.builder()
                    .id(accountId)
                    .name(accountName)
                    .balance(accountBalance)
                    .build()));

    AccountDto accountDto = accountService.getAccountDtoById(accountId);

    assertAll(
            () -> assertEquals(accountName, accountDto.getName()),
            () -> assertEquals(accountBalance, accountDto.getBalance())
    );
  }

  @Test
  void getAccountDtoById_whenGivenId_shouldThrowAccountNotFoundException() {
    Long accountId = 1L;
    when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

    assertThrows(AccountNotFoundException.class,
            () -> accountService.getAccountDtoById(accountId));
  }

  @Test
  void updateBalances_whenGivenAccountsAndAmount_shouldUpdate() {
    Account senderAccount = Account.builder()
            .balance(new BigDecimal("100.00"))
            .build();
    Account receiverAccount = Account.builder()
            .balance(new BigDecimal("50.00"))
            .build();
    BigDecimal amount = new BigDecimal("20.00");

    accountService.updateBalances(senderAccount, receiverAccount, amount);

    verify(accountRepository).save(senderAccount);
    verify(accountRepository).save(receiverAccount);

    assertEquals(new BigDecimal("80.00"), senderAccount.getBalance());
    assertEquals(new BigDecimal("70.00"), receiverAccount.getBalance());
  }

  @Test
  void validateSenderAccountBalance_WithSufficientBalance() {
    Account senderAccount = Account.builder()
            .balance(new BigDecimal("100.00"))
            .build();
    BigDecimal requestedAmount = new BigDecimal("50.00");

    assertDoesNotThrow(() -> accountService.validateSenderAccountBalance(senderAccount, requestedAmount));
  }

  @Test
  void validateSenderAccountBalance_whenInsufficientAmount_shouldThrowPaymentException() {
    Account senderAccount = Account.builder()
            .balance(new BigDecimal("100.00"))
            .build();
    BigDecimal requestedAmount = new BigDecimal("101.00");

    assertThrows(PaymentException.class,
            () -> accountService.validateSenderAccountBalance(senderAccount, requestedAmount));
  }
}