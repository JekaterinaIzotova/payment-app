package com.ridango.application.web.rest;

import com.ridango.application.db.service.AccountService;
import com.ridango.application.dto.AccountDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
@AllArgsConstructor
public class AccountController {
  private final AccountService accountService;

  @GetMapping("/{id}")
  public ResponseEntity<AccountDto> getAccountById(@PathVariable Long id) {
    return ResponseEntity.ok().body(accountService.getAccountDtoById(id));
  }
}
