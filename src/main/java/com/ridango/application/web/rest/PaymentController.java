package com.ridango.application.web.rest;

import com.ridango.application.db.service.PaymentService;
import com.ridango.application.dto.PaymentRequestDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payment")
@AllArgsConstructor
public class PaymentController {
  private final PaymentService paymentService;

  @PostMapping
  public ResponseEntity<Void> makePayment(@Validated @RequestBody PaymentRequestDto paymentRequestDto) {
    paymentService.makePayment(paymentRequestDto);
    return ResponseEntity.ok().build();
  }
}
