package com.ridango.application.dto;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

import static com.ridango.application.common.enumerations.ErrorDescription.AMOUNT_INCORRECT;
import static com.ridango.application.common.enumerations.ErrorDescription.AMOUNT_INVALID_FORMAT;
import static com.ridango.application.common.enumerations.ErrorDescription.CAN_NOT_BE_EMPTY;

@Getter
@Builder
public class PaymentRequestDto {
  @NotNull(message = CAN_NOT_BE_EMPTY)
  private Long senderAccountId;

  @NotNull(message = CAN_NOT_BE_EMPTY)
  private Long receiverAccountId;

  @NotNull(message = CAN_NOT_BE_EMPTY)
  @DecimalMin(value = "0.01", message = AMOUNT_INCORRECT)
  @Digits(integer = 16, fraction = 2, message = AMOUNT_INVALID_FORMAT)
  private BigDecimal amount;
}
