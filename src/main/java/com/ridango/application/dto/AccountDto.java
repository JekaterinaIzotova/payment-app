package com.ridango.application.dto;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@Builder
public class AccountDto {
  private String name;
  private BigDecimal balance;
}
