package com.ridango.application.common.enumerations;

public interface ErrorDescription {
  String ACCOUNT_NOT_FOUND = "Account not found";
  String INSUFFICIENT_BALANCE = "Insufficient balance in sender account";
  String AMOUNT_INVALID_FORMAT = "Invalid amount format";
  String AMOUNT_INCORRECT = "Amount must be greater than or equal to 0.01";
  String CAN_NOT_BE_EMPTY = "Can not be empty";
}