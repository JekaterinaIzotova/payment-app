package com.ridango.application.common.enumerations;

public enum AccountType {
  SENDER_ACCOUNT("senderAccountId"),
  RECEIVER_ACCOUNT("receiverAccountId");

  final String key;

  AccountType(String key) {
    this.key = key;
  }

  public String value() {
    return this.key;
  }
}
