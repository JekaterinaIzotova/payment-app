package com.ridango.application.common.exceptions;

import com.ridango.application.common.enumerations.AccountType;
import lombok.Getter;

@Getter
public class PaymentException extends PaymentRestException {
	public PaymentException(String description, AccountType accountType, Number value) {
		super(description, accountType, value);
	}
}
