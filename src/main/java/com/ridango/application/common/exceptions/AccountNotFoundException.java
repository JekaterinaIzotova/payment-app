package com.ridango.application.common.exceptions;

import com.ridango.application.common.enumerations.AccountType;
import lombok.Getter;

import static com.ridango.application.common.enumerations.ErrorDescription.ACCOUNT_NOT_FOUND;

@Getter
public class AccountNotFoundException extends PaymentRestException {
	public AccountNotFoundException(AccountType accountType, Number value) {
		super(ACCOUNT_NOT_FOUND, accountType, value);
	}
}
