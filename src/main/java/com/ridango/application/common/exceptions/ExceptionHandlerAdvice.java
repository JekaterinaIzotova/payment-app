package com.ridango.application.common.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ExceptionHandlerAdvice {
  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<List<Map<String, String>>> handleValidationExceptions(MethodArgumentNotValidException ex) {
    List<Map<String, String>> result = new ArrayList<>();

    ex.getBindingResult().getAllErrors().forEach((error) -> {
      result.add(getFormattedErrors(
              error.getDefaultMessage(),
              ((FieldError) error).getField(),
              (Number) ((FieldError) error).getRejectedValue()
      ));
    });

    return ResponseEntity.badRequest().body(result);
  }

  @ExceptionHandler(AccountNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<List<Map<String, String>>> handleAccountNotFoundException(AccountNotFoundException ex) {
    return getPaymentErrorResult(ex);
  }

  @ExceptionHandler(PaymentException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<List<Map<String, String>>> handlePaymentException(PaymentException ex) {
    return getPaymentErrorResult(ex);
  }

  private ResponseEntity<List<Map<String, String>>> getPaymentErrorResult(PaymentRestException ex) {
    List<Map<String, String>> result = new ArrayList<>();

    result.add(getFormattedErrors(
            ex.getDescription(),
            ex.getAccountType(),
            ex.getValue()
    ));

    return ResponseEntity.badRequest().body(result);
  }

  private static Map<String, String> getFormattedErrors(String description, String accountType, Number value) {
    Map<String, String> errors = new HashMap<>();

    errors.put("errorDescription", description);
    errors.put("type", accountType);
    errors.put("requestedValue", value == null ? null : value.toString());

    return errors;
  }
}
