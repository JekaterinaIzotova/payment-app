package com.ridango.application.common.exceptions;

import com.ridango.application.common.enumerations.AccountType;
import lombok.Getter;

@Getter
public class PaymentRestException extends RuntimeException {
	private final String description;
	private final String accountType;
	private final Number value;

	public PaymentRestException(String description, AccountType accountType, Number value) {
		super(description);
		this.description = description;
		this.accountType = accountType == null ? null : accountType.value();
		this.value = value;
	}
}
