package com.ridango.application.db.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Entity
@Getter
@Builder
public class Payment {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "sender_account_id")
  private Account senderAccount;

  @ManyToOne
  @JoinColumn(name = "receiver_account_id")
  private Account receiverAccount;

  @Column(name = "timestamp", nullable = false)
  private LocalDateTime timestamp;
}
