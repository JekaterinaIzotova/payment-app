package com.ridango.application.db.service;

import com.ridango.application.common.enumerations.AccountType;
import com.ridango.application.common.exceptions.AccountNotFoundException;
import com.ridango.application.common.exceptions.PaymentException;
import com.ridango.application.db.domain.Account;
import com.ridango.application.db.repository.AccountRepository;
import com.ridango.application.dto.AccountDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static com.ridango.application.common.enumerations.ErrorDescription.INSUFFICIENT_BALANCE;
import static com.ridango.application.common.enumerations.AccountType.SENDER_ACCOUNT;

@Service
@AllArgsConstructor
public class AccountService {
  private final AccountRepository accountRepository;

  public AccountDto getAccountDtoById(Long id) {
    return mapToDto(getAccountById(id, null));
  }

  Account getAccountById(Long id, AccountType accountType) {
    return accountRepository.findById(id)
            .orElseThrow(() -> new AccountNotFoundException(accountType, id));
  }

  void updateBalances(Account senderAccount, Account receiverAccount, BigDecimal amount) {
    senderAccount.setBalance(senderAccount.getBalance().subtract(amount));
    receiverAccount.setBalance(receiverAccount.getBalance().add(amount));

    accountRepository.save(senderAccount);
    accountRepository.save(receiverAccount);
  }

  void validateSenderAccountBalance(Account senderAccount, BigDecimal requestedAmount) {
    if (senderAccount.getBalance().compareTo(requestedAmount) < 0) {
      throw new PaymentException(INSUFFICIENT_BALANCE, SENDER_ACCOUNT, senderAccount.getId());
    }
  }

  private static AccountDto mapToDto(Account account) {
    return AccountDto.builder()
            .name(account.getName())
            .balance(account.getBalance())
            .build();
  }
}
