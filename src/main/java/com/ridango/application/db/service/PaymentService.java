package com.ridango.application.db.service;

import com.ridango.application.db.domain.Account;
import com.ridango.application.db.domain.Payment;
import com.ridango.application.db.repository.PaymentRepository;
import com.ridango.application.dto.PaymentRequestDto;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static com.ridango.application.common.enumerations.AccountType.RECEIVER_ACCOUNT;
import static com.ridango.application.common.enumerations.AccountType.SENDER_ACCOUNT;

@Service
@AllArgsConstructor
public class PaymentService {
  private final PaymentRepository paymentRepository;
  private final AccountService accountService;

  @Transactional
  public void makePayment(PaymentRequestDto paymentRequestDto) {
    val senderAccount = accountService.getAccountById(paymentRequestDto.getSenderAccountId(), SENDER_ACCOUNT);
    accountService.validateSenderAccountBalance(senderAccount, paymentRequestDto.getAmount());

    val receiverAccount = accountService.getAccountById(paymentRequestDto.getReceiverAccountId(), RECEIVER_ACCOUNT);

    accountService.updateBalances(senderAccount, receiverAccount, paymentRequestDto.getAmount());

    savePayment(senderAccount, receiverAccount);
  }

  private void savePayment(Account senderAccount, Account receiverAccount) {
    val payment = Payment.builder()
            .senderAccount(senderAccount)
            .receiverAccount(receiverAccount)
            .timestamp(LocalDateTime.now())
            .build();

    paymentRepository.save(payment);
  }
}
